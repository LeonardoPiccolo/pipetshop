
package DAO;

import Model.Cartoes;
import Model.Clientes;
import Utils.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sthefany
 */
//NÃO MECHER ESSE É O BASE !! //
public class ClienteDAO {
    
    static Connection conexao;

    static Conexao minhaConexao;
   
    
    
    
    
    public static boolean cadastrarCliente(Clientes cliente) {
        minhaConexao = new Conexao();       
        boolean status = false;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();
           
            String sql = "insert into Clientes ( nome, cpf, dataNascimento, sexo, senha, email, contato, StatusCliente, dataCadastro) "
                    + "values (?,?,?,?,?,?,?,?,getdate())";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, cliente.getNome());
            instrucaoSQL.setString(2, cliente.getCpf());
            instrucaoSQL.setString(3, cliente.getDataNascimento());
            instrucaoSQL.setString(4, cliente.getSexo());
            instrucaoSQL.setString(5, cliente.getSenha());
            instrucaoSQL.setString(6, cliente.getEmail());
            instrucaoSQL.setString(7, cliente.getContato());
            instrucaoSQL.setString(8, cliente.getStatusCliente());


            int linhasAfetadas = instrucaoSQL.executeUpdate();
            String sql2 = "select top 1 * from Clientes order by dataCadastro desc";
            // salvar variavel no retorno e mandar pro front

            if (linhasAfetadas > 0) {
                status = true;
                
            } else {
                throw new Exception();

            }

                        
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Erro ao iniciar conexão com o BD");
            
        }
        
        return status;
        
    }
    
    public static boolean alterarCliente(Clientes cliente) {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "UPDATE Clientes SET nome = ?, cpf = ?, dataNascimento = ?, sexo = ?, senha = ?, email = ?, contato = ?, StatusCliente = ? WHERE idCliente = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, cliente.getNome());
            instrucaoSQL.setString(2, cliente.getCpf());
            instrucaoSQL.setString(3, cliente.getDataNascimento());
            instrucaoSQL.setString(4, cliente.getSexo());
            instrucaoSQL.setString(5, cliente.getSenha());
            instrucaoSQL.setString(6, cliente.getEmail());
            instrucaoSQL.setString(7, cliente.getContato());
            instrucaoSQL.setString(8, cliente.getStatusCliente());
            instrucaoSQL.setInt(9, cliente.getIdCliente());
            
                    
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;

            } else {
                throw new Exception();

            }

            conexao.close();
            
        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");
            
        }
        
        return status;
        
    }
    
    public static boolean removerCliente(String cliente) {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "delete from Clientes where cpf = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, cliente);
            
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;

            } else {
                throw new Exception();

            }
            
        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");
            
        }
        
        return status;
        
    }
    
    public boolean validarCliente(Clientes cliente) {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "select * from Clientes where email = ?, senha = ?";

            instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, cliente.getEmail());
            instrucaoSQL.setString(2, cliente.getSenha());
            
            rs = instrucaoSQL.executeQuery();
            status = rs.next();
    
            if (rs != null) {
                rs.close();
                System.out.println("Cliente existe!");

            }

            if (instrucaoSQL != null) {
                instrucaoSQL.close();
                System.out.println("Cliente não existe.");
            }
            
            conexao.close();
            
            
        } catch (Exception e) {
            System.out.println("Erro na consulta.");
            
        }
        
        return status;
        
    }
    
    public static Clientes consultarCliente(String cliente) {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        Clientes cli = new Clientes();
        try {
            
           // Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "select * from Clientes where cpf = ?";
            
            instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setString(1, cliente);
            
            rs = instrucaoSQL.executeQuery();
            
            if (rs.next()) {
                
                cli.setIdCliente(rs.getInt("idCliente"));
                cli.setNome(rs.getString("nome"));
                cli.setCpf(rs.getString("cpf"));
                cli.setDataNascimento(rs.getString("dataNascimento"));
                cli.setSexo(rs.getString("sexo"));
                cli.setSenha(rs.getString("senha"));
                cli.setEmail(rs.getString("email"));
                cli.setContato(rs.getString("contato"));
                cli.setStatusCliente(rs.getString("statusCliente"));
            }
            
        } catch (Exception e) {
            System.out.println("Erro na consulta");
            
        } finally {
            
            try {

                if (rs != null) {
                    rs.close();
                    System.out.println("Cliente existe! Fim");

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                    System.out.println("Cliente não existe. Fim");

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }
            
        }
        
        return cli;
        
    }    
    
    public static ArrayList<Clientes> listarClientes() {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<Clientes> listaCliente = new ArrayList<Clientes>();
        
        try {
            
           // Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "select * from Clientes";

            instrucaoSQL = conexao.prepareStatement(sql);
            
            rs = instrucaoSQL.executeQuery();
            
            while (rs.next()) {
                
                Clientes cli = new Clientes();
                
                cli.setIdCliente(rs.getInt("idCliente"));
                cli.setNome(rs.getString("nome"));
                cli.setCpf(rs.getString("cpf"));
                cli.setDataNascimento(rs.getString("dataNascimento"));
                cli.setSexo(rs.getString("sexo"));
                cli.setSenha(rs.getString("senha"));
                cli.setEmail(rs.getString("email"));
                cli.setContato(rs.getString("contato"));
                cli.setStatusCliente(rs.getString("statusCliente"));
                listaCliente.add(cli);
                
            }
            
        } catch (Exception e) {
            System.out.println("Erro na listagem");
            
        } finally {
            
            try {

                if (rs != null) {
                    rs.close();

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }
            
        }
        
        return listaCliente;
        
    }
    
    public static int pegarId(Clientes cliente) {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        int idCliente = 0;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        
        try {
            
            // Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "select idCliente from Clientes where cpf = ?";
            
            instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, cliente.getCpf());
            
            rs = instrucaoSQL.executeQuery();
            
            if (rs.next()) {
                
                idCliente = rs.getInt("idCliente");
                
            }
            
        } catch (Exception e) {
            System.out.println("Erro na consulta");
            
        } finally {
            
            try {

                if (rs != null) {
                    rs.close();

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }
            
        }
        
        return idCliente;
        
    }

} 
