package Model;

import java.sql.Date;
import lombok.Getter;
import lombok.Setter;

public class Cartoes {
	private int idCartao;
	private String numeroCartao;
	private String nomeTitular;
    private String dataVencimento;
    private String CVV;
    
    public int getIdCartao() {
		return idCartao;
	}
	public void setIdCartao(int idCartao) {
		this.idCartao = idCartao;
	}
	public String getNumeroCartao() {
		return numeroCartao;
	}
	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}
    public String getNomeTitular() {
		return nomeTitular;
	}
	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}
    public String getDataVencimento() {
		return dataVencimento;
	}
	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	 public String getCvv() {
			return CVV;
	}
	public void setCvv(String CVV) {
		this.CVV = CVV;
	}
}

    