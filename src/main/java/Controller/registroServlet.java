

package Controller;

import java.util.ArrayList;
import java.util.List;
import Model.Clientes;
import Model.Enderecos;
import DAO.ClienteDAO;
import DAO.EnderecoDAO;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="registro", urlPatterns={"/registroServlet"})
public class registroServlet extends HttpServlet {

    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    	
        Clientes cliente = new Clientes();
        Enderecos endereco = new Enderecos();
        
        ClienteDAO dao = new ClienteDAO();
        EnderecoDAO daoend = new EnderecoDAO();
        
        cliente.setNome(request.getParameter("nome"));
        cliente.setEmail(request.getParameter("email"));
        cliente.setContato(request.getParameter("fone"));
        cliente.setSenha(request.getParameter("senha"));
        cliente.setCpf(request.getParameter("CPF"));
        cliente.setDataNascimento(request.getParameter("dataNascimento"));
        cliente.setSexo(request.getParameter("sexo"));
        cliente.setStatusCliente("ativo");
        
        endereco.setCep(request.getParameter("CEP"));
        endereco.setEndereco(request.getParameter("endereco"));
        endereco.setBairro(request.getParameter("bairro"));
        endereco.setCidade(request.getParameter("cidade"));
        endereco.setUf(request.getParameter("UF"));
        
        dao.cadastrarCliente(cliente);
        daoend.cadastrarEndereco(endereco);
        
        response.sendRedirect("inicio.jsp");
    }

}
