

package Controller;

import java.util.ArrayList;
import java.util.List;
import Model.Produtos;
import DAO.ProdutoDAO;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="registro", urlPatterns={"/produtoServlet"})
public class produtoServlet extends HttpServlet {

    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    	
        Produtos produto = new Produtos();
        
        ProdutoDAO proddao = new ProdutoDAO();
        
        produto.setNomeProduto(request.getParameter("nome"));
        produto.setRaca(request.getParameter("raca"));
        produto.setValorEntrada(request.getParameter("valorEnt"));
        produto.setValorVenda(request.getParameter("valorVend"));
        produto.setStatusProduto(request.getParameter("status"));
        produto.setDescricao(request.getParameter("desc"));
        produto.setDataRegistro(request.getParameter("registro"));
        produto.setQuantidade(request.getParameter("quantidade"));
        produto.setFornecedor(request.getParameter("fornecedor"));
        
        proddao.cadastrarProdutos(produto);
        
        response.sendRedirect("inicio.jsp");
    }

}
