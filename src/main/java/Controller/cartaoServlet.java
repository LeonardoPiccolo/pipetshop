
package Controller;

import java.util.ArrayList;
import java.util.List;

import Model.Cartoes;
import Model.Clientes;
import Model.Enderecos;
import DAO.ClienteDAO;
import DAO.EnderecoDAO;
import DAO.CartaoDAO;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="cartao", urlPatterns={"/cartaoServlet"})
public class cartaoServlet extends HttpServlet {

    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    	
        Cartoes cartao = new Cartoes();
       
        CartaoDAO clidao = new CartaoDAO();
        
        
        cartao.setNomeTitular(request.getParameter("nome-titular"));
        cartao.setNumeroCartao(request.getParameter("numero-cartao"));
        cartao.setDataVencimento(request.getParameter("data-vencimento"));
        cartao.setCvv(request.getParameter("cvv"));
        
        clidao.cadastrarCartao(cartao);
        
        response.sendRedirect("carrinho.jsp");
    }

}
